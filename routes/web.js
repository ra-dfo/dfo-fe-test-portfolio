const express = require('express');
const router = express.Router();

// The main route
router.get('/', (request, response) => {
  response.setHeader('X-Robots-Tag', 'noindex, nofollow')
  response.render('index', {
    title: 'DFO FE Test Portfolio'
  })
});

module.exports = router;
