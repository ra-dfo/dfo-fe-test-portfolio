# DFO FE Test Portoflio

This small portfolio is part of the DFO FE Test 2021, using NodeJS, express, nodemon, animate.css, bootstrap 4, and Pug

## Installation

1. Clone this repository.
1. Run ```npm install``` in your terminal to get all needed packages. 
1. Run ```npm run dev``` to start the development mode.

Note: There is only the development mode, for testing purpouses.

## Routing

The routing is done in the __/routes/web.js__ file.  

## Views

The views are located in the __/views/__ directory using [Pug](https://github.com/pugjs/pug) as template engine.


## License

Project under [MIT License](./License.md) and is free for private and commercial usage.